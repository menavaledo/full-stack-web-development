$(function () {
        $('[data-bs-toggle="tooltip"]').tooltip()
        $('[data-bs-toggle="popover"]').popover()
      
        $('#myModalForm').on('show.bs.modal', function (e){
          console.log('el modal contacto se esta mostrando')

          $('#contactoBtn').removeClass('btn-outline-info');
          $('#contactoBtn').addClass('btn-info');
          $('#contactoBtn').prop('disabled', true);
  

        });
        $('#myModalForm').on('shown.bs.modal', function (e){
          console.log('el modal contacto se mostró')
        });
        $('#myModalForm').on('hide.bs.modal', function (e){
          console.log('el modal contacto se oculta')
        });
        $('#myModalForm').on('hidden.bs.modal', function (e){
          console.log('el modal contacto se ocultó')
      
          $('#contactoBtn').prop('disabled', false);
          $('#contactoBtn').removeClass('btn-info');
          $('#contactoBtn').addClass('btn-outline-info');
        });

      });